﻿using Hjklvfr.Sueta.Abstractions.Models;

namespace Hjklvfr.Sueta.Api.Abstractions.ResponseModels
{
    public class ProxyResponseModel
    {
        public string Ip { get; set; }
        
        public int Port { get; set; }
        
        public string Protocol { get; set; }

        public static ProxyResponseModel FromProxyModel(ProxyModel proxyModel)
        {
            return new()
            {
                Ip = proxyModel.Ip.ToString(),
                Port = proxyModel.Port,
                Protocol = proxyModel.Protocol.ToString()
            };
        }
    }
}