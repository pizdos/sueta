﻿using System.Collections.Generic;

namespace Hjklvfr.Sueta.Api.Abstractions.ResponseModels
{
    public class ProxyListResponseModel
    {
        public int Length { get; set; }
        
        public IEnumerable<ProxyResponseModel> ProxyResponseModels { get; set; }
    }
}