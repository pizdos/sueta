﻿using System.Threading.Tasks;
using Hjklvfr.Sueta.Abstractions.Models;

namespace Hjklvfr.Sueta.Pool.Abstractions
{
    public interface IPoolService
    {
        Task<(bool, ProxyModel)> TryGetNextProxyAsync();
        
        Task<ProxyModel> GetNextProxyAsync();
    }
}