﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Pool.Abstractions.Stores.Models;

namespace Hjklvfr.Sueta.Pool.Abstractions.Stores
{
    public interface IPoolRepository
    {
        Task<List<Protocol>> GetProtocolsAsync();
        
        Task<Protocol> GetProtocolByIdAsync(string protocolId);
        
        Task<List<Proxy>> GetProxiesAsync();
        
        Task<List<Proxy>> GetProxiesByProtocolAsync(Protocol protocol);

        Task<Proxy> GetProxyByIpAndPortAsync(string ip, int port);

        Task<Proxy> DeleteProxyAsync(Proxy proxy);
        
        Task<Proxy> UpdateProxyAsync(Proxy proxy);
        
        Task<Proxy> AddProxyAsync(Proxy proxy);

        Task<Proxy> TryAddProxyAsync(Proxy proxy);

        Task<int> AddProxiesAsync(List<Proxy> proxies);
    }
}