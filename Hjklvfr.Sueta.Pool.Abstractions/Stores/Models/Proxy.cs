﻿namespace Hjklvfr.Sueta.Pool.Abstractions.Stores.Models
{
    public partial class Proxy
    {
        public string ProxyId { get; set; }
        
        public string Ip { get; set; }
        
        public int Port { get; set; }
        
        public string ProtocolId { get; set; }
        
        public virtual Protocol Protocol { get; set; }
    }
}