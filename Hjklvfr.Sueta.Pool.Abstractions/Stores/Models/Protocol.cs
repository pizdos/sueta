﻿namespace Hjklvfr.Sueta.Pool.Abstractions.Stores.Models
{
    public partial class Protocol
    {
        public string ProtocolId { get; set; }
        
        public string Name { get; set; }
    }
}