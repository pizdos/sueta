﻿using System;
using Hjklvfr.Sueta.Checker.Extensions;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Extensions;
using Hjklvfr.Sueta.Pool.BackgroundSearcher.Extensions;
using Hjklvfr.Sueta.Pool.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.Sueta.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSueta(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> poolOptionsAction,
            IConfiguration backgroundSearcherConfiguration)
        {
            services.AddProxyChecker();
            services.AddProxyScrapeComGrabber();
            services.AddPoolService(poolOptionsAction);
            services.AddBackgroundSearcher(backgroundSearcherConfiguration);
            return services;
        }
    }
}