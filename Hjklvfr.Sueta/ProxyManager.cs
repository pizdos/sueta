﻿using System;
using System.Net;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Abstractions;
using Hjklvfr.Sueta.Pool.Abstractions;

namespace Hjklvfr.Sueta
{
    public class ProxyManager : IProxyManager
    {
        private readonly IPoolService _poolService;

        public ProxyManager(IPoolService poolService)
        {
            _poolService = poolService;
        }

        public async Task<IWebProxy> GetProxy()
        {
            var proxyModel = await _poolService.GetNextProxyAsync();

            var uri = new UriBuilder()
            {
                Host = proxyModel.Ip.ToString(),
                Port = proxyModel.Port
            }.Uri;

            return new WebProxy(uri);
        }
    }
}