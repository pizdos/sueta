﻿namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions.RequestModels
{
    public abstract class RequestModelBase
    {
        protected RequestModelBase(string request)
        {
            Request = request;
        }

        public string Request { get; set; }
    }
}