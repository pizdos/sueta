﻿namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions.RequestModels
{
    public class DisplayProxiesRequestModel : RequestModelBase
    {
        public string ProxyType { get; set; }

        public int Timeout { get; set; }

        public string Ssl { get; set; }

        public string Anonymity { get; set; }

        public string Country { get; set; } = "all";

        public string Limit { get; } = "all";

        public string Format { get; set; }

        public string SerialKey { get; set; }

        public string Age { get; set; } = "unlimited";

        public string Status { get; set; } = "alive";

        public int? AverageTimeout { get; set; } // null because use free plan

        public string Port { get; } = "all";

        public DisplayProxiesRequestModel() : base("displayproxies")
        {
        }
    }
}