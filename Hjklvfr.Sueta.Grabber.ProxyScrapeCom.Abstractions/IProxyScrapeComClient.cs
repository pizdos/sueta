﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions.RequestModels;

namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions
{
    public interface IProxyScrapeComClient
    {
        Task<IEnumerable<string>> GetProxies(DisplayProxiesRequestModel requestModel);
    }
}