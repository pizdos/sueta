﻿namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Constants
{
    public static class AnonymityConstants
    {
        public static string Elite = "elite";

        public static string Anonymous = "anonymous";

        public static string Transparent = "transparent";

        public static string All = "all";
    }
}