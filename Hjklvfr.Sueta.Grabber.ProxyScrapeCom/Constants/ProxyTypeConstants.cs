﻿namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Constants
{
    public static class ProxyTypeConstants
    {
        public static string Http = "http";

        public static string Socks4 = "socks4";

        public static string Socks5 = "socks5";
    }
}