﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions.RequestModels;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Extensions;

namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom
{
    public class ProxyScrapeComClient : IProxyScrapeComClient
    {
        private readonly HttpClient _httpClient;

        public static Uri BaseAddress { get; } = new("https://api.proxyscrape.com/");

        public ProxyScrapeComClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<string>> GetProxies(DisplayProxiesRequestModel requestModel)
        {
            var response = await _httpClient.GetAsync($"{BaseAddress}?{requestModel.ToQueryString()}");
            response.EnsureSuccessStatusCode();
            var responseList = await response.Content.ReadAsStringAsync();
            return responseList.Split("\r\n").Where(x => !string.IsNullOrEmpty(x)).ToArray();
        }
    }
}