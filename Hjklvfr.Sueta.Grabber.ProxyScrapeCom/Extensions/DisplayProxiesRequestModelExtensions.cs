﻿using System;
using System.Linq;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions.RequestModels;

namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Extensions
{
    public static class DisplayProxiesRequestModelExtensions
    {
        public static string ToQueryString(this DisplayProxiesRequestModel displayProxiesRequestModel)
        {
            var properties = displayProxiesRequestModel.GetType().GetProperties()
                .Where(p => p.GetValue(displayProxiesRequestModel, null) != null)
                .Select(p =>
                    $"{Uri.EscapeDataString(p.Name.ToLower())}={Uri.EscapeDataString(p.GetValue(displayProxiesRequestModel)?.ToString() ?? string.Empty)}");
            return string.Join('&', properties.ToArray());
        }
    }
}