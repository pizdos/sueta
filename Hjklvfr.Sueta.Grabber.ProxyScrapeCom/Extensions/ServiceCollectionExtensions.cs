﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Hjklvfr.Sueta.Grabber.Abstractions;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddProxyScrapeComGrabber(this IServiceCollection services)
        {
            services.AddHttpClient<IProxyScrapeComClient, ProxyScrapeComClient>(client =>
            {
                client.BaseAddress = ProxyScrapeComClient.BaseAddress;
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }).ConfigurePrimaryHttpMessageHandler(_ => new HttpClientHandler
            {
                AutomaticDecompression = (DecompressionMethods.GZip | DecompressionMethods.Deflate)
            });
            services.AddScoped<IProxyGrabber, ProxyScrapeComGrabber>();
            return services;
        }
    }
}