﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Abstractions.Models;
using Hjklvfr.Sueta.Grabber.Abstractions;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions.RequestModels;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Constants;

namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom
{
    public class ProxyScrapeComGrabber : IProxyGrabber
    {
        private readonly IProxyScrapeComClient _proxyScrapeComClient;

        public ProxyScrapeComGrabber(IProxyScrapeComClient proxyScrapeComClient)
        {
            _proxyScrapeComClient = proxyScrapeComClient;
        }

        public async Task<IEnumerable<ProxyModel>> FindProxies()
        {
            var requestModel = new DisplayProxiesRequestModel()
            {
                ProxyType = ProxyTypeConstants.Http,
                Anonymity = AnonymityConstants.All,
                AverageTimeout = null,
                Format = FormatConstants.Normal,
                SerialKey = null,
                Ssl = SslConstants.Yes,
                Timeout = 500
            };

            var proxies = await _proxyScrapeComClient.GetProxies(requestModel);
            return proxies.Select(s =>
            {
                var split = s.Split(':');
                return new ProxyModel()
                {
                    Ip = IPAddress.Parse(split[0]),
                    Port = int.Parse(split[1]),
                    Protocol = Protocol.Http
                };
            }).ToList();
        }
    }
}