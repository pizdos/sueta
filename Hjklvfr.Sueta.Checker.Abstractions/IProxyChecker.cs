﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Abstractions.Models;

namespace Hjklvfr.Sueta.Checker.Abstractions
{
    public interface IProxyChecker
    {
        Task<bool> IsProxyAliveAsync(ProxyModel proxyModel);

        Task<IEnumerable<ProxyModel>> AliveProxiesFromListAsync(IEnumerable<ProxyModel> proxyModels);

        Task AliveProxiesCallbackAsync(IEnumerable<ProxyModel> proxyModels, Func<ProxyModel, Task> callbackAsync);
    }
}