﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Threading.Tasks.Schedulers;
using Hjklvfr.Sueta.Abstractions.Models;
using Hjklvfr.Sueta.Checker.Abstractions;
using Newtonsoft.Json;

namespace Hjklvfr.Sueta.Checker
{
    public class ProxyChecker : IProxyChecker
    {
        private class IpApiResponse
        {
            private readonly string _city;
            private readonly string _query;

            public IpApiResponse(string city, string query)
            {
                _city = city;
                _query = query;
            }
        }
        
        public async Task<bool> IsProxyAliveAsync(ProxyModel proxyModel)
        {
            //Debug.WriteLine("huy");
            var request = WebRequest.CreateHttp("http://ip-api.com/json?fields=city,query");
            request.Timeout = 1000; //todo: to config checker timeout millis
            var webProxy = new WebProxy(proxyModel.Ip.ToString(), proxyModel.Port) {BypassProxyOnLocal = false};
            request.Proxy = webProxy;
            request.Method = "GET";
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse) await request.GetResponseAsync();
            }
            catch (WebException)
            {
                return false;
            }

            var responseStream = response.GetResponseStream();
            var streamReader = new StreamReader(responseStream);
            var responseText = await streamReader.ReadToEndAsync();
            try
            {
                var ipApiResponse = JsonConvert.DeserializeObject<IpApiResponse>(responseText);
            }
            catch (JsonSerializationException)
            {
                return false;
            }

            return response.StatusCode == HttpStatusCode.OK;
        }

        public async Task AliveProxiesCallbackAsync(IEnumerable<ProxyModel> proxyModels, Func<ProxyModel, Task> callbackAsync)
        {
            var models = proxyModels.ToList();
            var workStealingTaskScheduler = new WorkStealingTaskScheduler(20);
            var taskFactory = new TaskFactory(default, TaskCreationOptions.None, TaskContinuationOptions.None, workStealingTaskScheduler);

            var tasks = models.Select(proxy =>
            {
                return taskFactory.StartNew(() =>
                {
                    if (IsProxyAliveAsync(proxy).Result)
                        callbackAsync.Invoke(proxy).Wait();
                });
            });
            
            await Task.WhenAll(tasks.ToArray());
        }

        public async Task<IEnumerable<ProxyModel>> AliveProxiesFromListAsync(IEnumerable<ProxyModel> proxyModels)
        {
            var models = proxyModels.ToList();
            var notAlive = new ConcurrentBag<ProxyModel>();
            var workStealingTaskScheduler = new WorkStealingTaskScheduler(20);
            var taskFactory = new TaskFactory(default, TaskCreationOptions.None, TaskContinuationOptions.None, workStealingTaskScheduler);
            var tasks = models.Select(fp =>
            {
                return taskFactory.StartNew(() =>
                {
                    if (!IsProxyAliveAsync(fp).Result) 
                        notAlive.Add(fp);
                });
            });
            await Task.WhenAll(tasks.ToArray());

            models.RemoveAll(fp => notAlive.Contains(fp));
            return models;
        }
    }
}