﻿using Hjklvfr.Sueta.Checker.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.Sueta.Checker.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddProxyChecker(this IServiceCollection services)
        {
            services.AddScoped<IProxyChecker, ProxyChecker>();
            return services;
        }
    }
}