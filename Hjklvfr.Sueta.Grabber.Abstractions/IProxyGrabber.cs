﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Abstractions.Models;

namespace Hjklvfr.Sueta.Grabber.Abstractions
{
    public interface IProxyGrabber
    {
        Task<IEnumerable<ProxyModel>> FindProxies();
    }
}