﻿using System.Threading.Tasks;
using Hjklvfr.Sueta.Api.Abstractions.ResponseModels;
using Hjklvfr.Sueta.Pool.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Hjklvfr.Sueta.Pool.Api.Controllers
{
    [ApiController]
    [Route("v1/pool")]
    public class PoolController : ControllerBase
    {
        private readonly IPoolService _poolService;

        public PoolController(
            IPoolService poolService)
        {
            _poolService = poolService;
        }

        [HttpGet("next")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ProxyResponseModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetProxy()
        {
            var proxy = await _poolService.GetNextProxyAsync();
            if (proxy == null)
                return NotFound();
            return Ok(ProxyResponseModel.FromProxyModel(proxy));
        }
    }
}