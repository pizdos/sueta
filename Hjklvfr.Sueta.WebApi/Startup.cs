using Hjklvfr.Sueta.Checker.Extensions;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Extensions;
using Hjklvfr.Sueta.Pool.BackgroundSearcher.Extensions;
using Hjklvfr.Sueta.Pool.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Hjklvfr.Sueta.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddProxyChecker();
            services.AddProxyScrapeComGrabber();
            services.AddPoolService(options =>
                options.UseLazyLoadingProxies().UseNpgsql(Configuration.GetConnectionString("DbConnection"), 
                    b => b.MigrationsAssembly("Hjklvfr.Sueta.WebApi")));
            services.AddBackgroundSearcher(Configuration.GetSection("BackgroundSearcher"));
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Hjklvfr.Sueta.WebApi", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Hjklvfr.Sueta.WebApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}