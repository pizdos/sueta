﻿using System.Linq;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Api.Abstractions.ResponseModels;
using Hjklvfr.Sueta.Checker.Abstractions;
using Hjklvfr.Sueta.Grabber.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace Hjklvfr.Sueta.Api.Controllers
{
    [ApiController]
    [Route("v1/proxy")]
    public class ProxyController : ControllerBase
    {
        private readonly IProxyGrabber _proxyGrabber;
        private readonly IProxyChecker _proxyChecker;

        public ProxyController(
            IProxyGrabber proxyGrabber,
            IProxyChecker proxyChecker)
        {
            _proxyGrabber = proxyGrabber;
            _proxyChecker = proxyChecker;
        }

        [HttpGet("alive")]
        public async Task<ProxyListResponseModel> GetAliveProxies()
        {
            var proxyResponseModels = (await _proxyChecker.AliveProxiesFromListAsync(
                    await _proxyGrabber.FindProxies()))
                .Select(ProxyResponseModel.FromProxyModel).ToList();
            return new ProxyListResponseModel()
            {
                Length = proxyResponseModels.Count,
                ProxyResponseModels = proxyResponseModels
            };
        }
    }
}