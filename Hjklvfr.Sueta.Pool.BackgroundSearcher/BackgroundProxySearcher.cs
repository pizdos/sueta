﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Checker.Abstractions;
using Hjklvfr.Sueta.Grabber.Abstractions;
using Hjklvfr.Sueta.Pool.Abstractions.Stores;
using Hjklvfr.Sueta.Pool.Abstractions.Stores.Models;
using Hjklvfr.Sueta.Pool.BackgroundSearcher.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Hjklvfr.Sueta.Pool.BackgroundSearcher
{
    public class BackgroundProxySearcher : BackgroundService
    {
        private readonly ILogger<BackgroundProxySearcher> _logger;
        private readonly BackgroundProxySearcherOptions _options;

        private readonly IServiceProvider _services;

        public BackgroundProxySearcher(
            ILogger<BackgroundProxySearcher> logger,
            IOptions<BackgroundProxySearcherOptions> options,
            IServiceProvider services)
        {
            _logger = logger;
            _options = options.Value;
            _services = services;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            if (!_options.ProxySearcherEnabled)
            {
                _logger.LogInformation("BackgroundProxySearcher disabled by config");
                return Task.CompletedTask;
            }

            _logger.LogInformation("BackgroundProxySearcher starting");

            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    _logger.LogInformation("BackgroundProxySearcher DoWork");
                    await FindAndCheckAndStore();
                }
                catch (Exception ex)
                {
                    _logger.LogCritical(ex, "BackgroundProxySearcher some error");
                }

                _logger.LogInformation("BackgroundProxySearcher Delay {DelayInMinutes} minutes",
                    _options.DelayInMinutes);
                await Task.Delay(TimeSpan.FromMinutes(_options.DelayInMinutes), stoppingToken);
            }
        }

        private async Task FindAndCheckAndStore()
        {
            var scope = _services.CreateScope();
            var proxyGrabber = scope.ServiceProvider.GetRequiredService<IProxyGrabber>();
            var proxyChecker = scope.ServiceProvider.GetRequiredService<IProxyChecker>();

            var proxies = await proxyGrabber.FindProxies();
            await proxyChecker.AliveProxiesCallbackAsync(proxies, async (proxy) =>
            {
                var callbackScope = scope.ServiceProvider.CreateScope();
                var poolRepository = callbackScope.ServiceProvider.GetRequiredService<IPoolRepository>();
                await poolRepository.TryAddProxyAsync(new Proxy()
                {
                    ProxyId = Guid.NewGuid().ToString(),
                    Ip = proxy.Ip.ToString(),
                    Port = proxy.Port,
                    ProtocolId = proxy.Protocol.ToString().ToLower()
                });
            });
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("BackgroundProxySearcher stopping");

            return base.StopAsync(cancellationToken);
        }
    }
}