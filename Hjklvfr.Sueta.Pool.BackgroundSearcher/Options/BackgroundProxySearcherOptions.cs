﻿namespace Hjklvfr.Sueta.Pool.BackgroundSearcher.Options
{
    public class BackgroundProxySearcherOptions
    {
        public bool ProxySearcherEnabled { get; set; }
        public int DelayInMinutes { get; set; }
    }
}