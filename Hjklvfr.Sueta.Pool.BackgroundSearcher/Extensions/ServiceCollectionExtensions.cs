﻿using Hjklvfr.Sueta.Pool.BackgroundSearcher.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.Sueta.Pool.BackgroundSearcher.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddBackgroundSearcher(
            this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<BackgroundProxySearcherOptions>(configuration);
            services.AddHostedService<BackgroundProxySearcher>();
            return services;
        }
    }
}