﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Abstractions.Models;
using Hjklvfr.Sueta.Checker.Abstractions;
using Hjklvfr.Sueta.Pool.Abstractions;
using Hjklvfr.Sueta.Pool.Abstractions.Stores;
using Hjklvfr.Sueta.Pool.Abstractions.Stores.Models;
using Hjklvfr.Sueta.Pool.Extensions;

namespace Hjklvfr.Sueta.Pool
{
    public class PoolService : IPoolService
    {
        private readonly IPoolRepository _poolRepository;
        private readonly IProxyChecker _proxyChecker;

        public PoolService(
            IPoolRepository poolRepository, 
            IProxyChecker proxyChecker)
        {
            _poolRepository = poolRepository;
            _proxyChecker = proxyChecker;
        }

        public async Task<(bool, ProxyModel)> TryGetNextProxyAsync()
        {
            var protocol = await _poolRepository.GetProtocolByIdAsync("http");
            var proxies = await _poolRepository.GetProxiesByProtocolAsync(protocol);
            var random = new Random();
            var proxy = proxies[random.Next(proxies.Count)].ToProxyModel();
            return (await _proxyChecker.IsProxyAliveAsync(proxy), proxy);
        }

        public async Task<ProxyModel> GetNextProxyAsync()
        {
            var protocol = await _poolRepository.GetProtocolByIdAsync("http");
            var proxies = await _poolRepository.GetProxiesByProtocolAsync(protocol);
            var random = new Random();
            var proxyModels = proxies.Select(p => p.ToProxyModel()).ToList();

            var iterCounter = 1;
            while (true)
            {
                var proxyModel = proxyModels[random.Next(proxyModels.Count)];
                if (await _proxyChecker.IsProxyAliveAsync(proxyModel))
                    return proxyModel;
                if (iterCounter > 30)//todo: to config count of spin iters
                    return null;
                iterCounter++;
            }
        }
    }
}