﻿using System;
using System.Linq;
using System.Net;
using Hjklvfr.Sueta.Abstractions.Models;
using Hjklvfr.Sueta.Pool.Abstractions.Stores.Models;
using Protocol = Hjklvfr.Sueta.Abstractions.Models.Protocol;

namespace Hjklvfr.Sueta.Pool.Extensions
{
    public static class ProxyExtensions
    {
        public static ProxyModel ToProxyModel(this Proxy proxy) =>
            new()
            {
                Ip = IPAddress.Parse(proxy.Ip),
                Port = proxy.Port,
                Protocol = (Protocol) Enum.Parse(typeof(Protocol), proxy.ProtocolId.First().ToString().ToUpper() + proxy.ProtocolId.Substring(1))
            };
    }
}