﻿using System;
using Hjklvfr.Sueta.Pool.Abstractions;
using Hjklvfr.Sueta.Pool.Abstractions.Stores;
using Hjklvfr.Sueta.Pool.EntityFrameworkCore;
using Hjklvfr.Sueta.Pool.EntityFrameworkCore.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Hjklvfr.Sueta.Pool.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddPoolService(this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionsAction = null)
        {
            services.AddDbContext<PoolDbContext>(optionsAction);
            services.AddTransient<IPoolRepository, PoolRepository>();
            services.AddScoped<IPoolService, PoolService>();
            return services;
        }
    }
}