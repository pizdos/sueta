﻿using Hjklvfr.Sueta.Pool.Abstractions.Stores.Models;
using Hjklvfr.Sueta.Pool.EntityFrameworkCore.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Hjklvfr.Sueta.Pool.EntityFrameworkCore.Contexts
{
    public class PoolDbContext : DbContext
    {
        public PoolDbContext(DbContextOptions<PoolDbContext> options) : base(options)
        {
        }
        
        public virtual DbSet<Proxy> Proxies { get; set; }
        
        public virtual DbSet<Protocol> Protocols { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProxyConfiguration());
            modelBuilder.ApplyConfiguration(new ProtocolConfiguration());

            modelBuilder.Entity<Protocol>()
                .HasData(GetProtocolData());
        }

        public static Protocol[] GetProtocolData()
        {
            return new[]
            {
                new Protocol()
                {
                    ProtocolId = "http",
                    Name = "Http",
                },
                new Protocol()
                {
                    ProtocolId = "socks4",
                    Name = "Socks4",
                },
                new Protocol()
                {
                    ProtocolId = "socks5",
                    Name = "Socks5"
                }
            };
        }
    }
}