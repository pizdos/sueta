﻿using Hjklvfr.Sueta.Pool.Abstractions.Stores.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hjklvfr.Sueta.Pool.EntityFrameworkCore.Configurations
{
    public class ProxyConfiguration : IEntityTypeConfiguration<Proxy>
    {
        public void Configure(EntityTypeBuilder<Proxy> builder)
        {
            builder
                .ToTable("proxies", "proxy_pool");

            builder
                .HasKey(p => new
                {
                    p.ProxyId,
                    p.Ip,
                    p.Port
                }).HasName("proxy_pkey");

            builder
                .HasIndex(p => new
                {
                    p.Ip,
                    p.Port
                }).IsUnique();

            builder.Property(p => p.ProxyId)
                .HasColumnName("proxy_id")
                .HasMaxLength(36)
                .ValueGeneratedNever();

            builder.Property(p => p.Ip)
                .HasColumnName("ip")
                .HasMaxLength(16);
            
            builder.Property(p => p.Port)
                .HasColumnName("port");
            
            builder.Property(p => p.ProtocolId)
                .HasColumnName("protocol_id")
                .HasMaxLength(10)
                .ValueGeneratedNever();
        }
    }
}