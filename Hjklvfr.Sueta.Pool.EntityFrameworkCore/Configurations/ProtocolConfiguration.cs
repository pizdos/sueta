﻿using Hjklvfr.Sueta.Pool.Abstractions.Stores.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Hjklvfr.Sueta.Pool.EntityFrameworkCore.Configurations
{
    public class ProtocolConfiguration : IEntityTypeConfiguration<Protocol>
    {
        public void Configure(EntityTypeBuilder<Protocol> builder)
        {
            builder
                .ToTable("protocols", "proxy_pool");
            
            builder
                .HasKey(p => p.ProtocolId)
                .HasName("protocol_pkey");

            builder.Property(p => p.ProtocolId)
                .HasColumnName("protocol_id")
                .HasMaxLength(10)
                .ValueGeneratedNever();

            builder.Property(p => p.Name)
                .HasColumnName("name")
                .HasMaxLength(10);
        }
    }
}