﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Pool.Abstractions.Stores;
using Hjklvfr.Sueta.Pool.Abstractions.Stores.Models;
using Hjklvfr.Sueta.Pool.EntityFrameworkCore.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Hjklvfr.Sueta.Pool.EntityFrameworkCore
{
    public class PoolRepository : IPoolRepository
    {
        private readonly PoolDbContext _poolDbContext;

        public PoolRepository(PoolDbContext poolDbContext)
        {
            _poolDbContext = poolDbContext;
        }

        public Task<List<Protocol>> GetProtocolsAsync()
        {
            return _poolDbContext.Protocols.AsNoTracking().ToListAsync();
        }

        public Task<Protocol> GetProtocolByIdAsync(string protocolId)
        {
            return _poolDbContext.Protocols.AsNoTracking().FirstOrDefaultAsync(p => p.ProtocolId == protocolId);
        }

        public Task<List<Proxy>> GetProxiesAsync()
        {
            return _poolDbContext.Proxies.AsNoTracking().ToListAsync();
        }

        public Task<List<Proxy>> GetProxiesByProtocolAsync(Protocol protocol)
        {
            return _poolDbContext.Proxies.AsNoTracking().Where(p => p.Protocol == protocol).ToListAsync();
        }

        public Task<Proxy> GetProxyByIpAndPortAsync(string ip, int port)
        {
            return _poolDbContext.Proxies.AsNoTracking()
                .FirstOrDefaultAsync(p =>
                    p.Ip == ip &&
                    p.Port == port);
        }

        public async Task<Proxy> DeleteProxyAsync(Proxy proxy)
        {
            _poolDbContext.Proxies.Remove(proxy);
            await _poolDbContext.SaveChangesAsync();
            return proxy;
        }

        public async Task<Proxy> UpdateProxyAsync(Proxy proxy)
        {
            _poolDbContext.Proxies.Update(proxy);
            await _poolDbContext.SaveChangesAsync();
            return proxy;
        }

        public async Task<Proxy> AddProxyAsync(Proxy proxy)
        {
            await _poolDbContext.Proxies.AddAsync(proxy);
            await _poolDbContext.SaveChangesAsync();
            return proxy;
        }

        public async Task<Proxy> TryAddProxyAsync(Proxy proxy)
        {
            var res = await _poolDbContext.Proxies.FirstOrDefaultAsync(p => p.Ip.Equals(proxy.Ip) && p.Port.Equals(p.Port));
            if (res != default) return null;
            await _poolDbContext.Proxies.AddAsync(proxy);
            await _poolDbContext.SaveChangesAsync();
            return proxy;
        }

        public async Task<int> AddProxiesAsync(List<Proxy> proxies)
        {
            var proxiesInDb = await _poolDbContext.Proxies.ToListAsync();
            await _poolDbContext.Proxies.AddRangeAsync(
                proxies.Where(p =>
                    proxiesInDb.FirstOrDefault(pdb =>
                        pdb.Ip == p.Ip && pdb.Port == p.Port) == default));
            await _poolDbContext.SaveChangesAsync();
            return proxies.Count;
        }
    }
}