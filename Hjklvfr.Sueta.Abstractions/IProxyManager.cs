﻿using System.Net;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Abstractions.Models;

namespace Hjklvfr.Sueta.Abstractions
{
    public interface IProxyManager
    {
        Task<IWebProxy> GetProxy();
    }
}