﻿namespace Hjklvfr.Sueta.Abstractions.Models
{
    public enum Protocol
    {
        Http,
        Socks4,
        Socks5
    }
}