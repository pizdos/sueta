﻿using System.Net;

namespace Hjklvfr.Sueta.Abstractions.Models
{
    public class ProxyModel
    {
        public IPAddress Ip { get; set; }
        
        public int Port { get; set; }
        
        public Protocol Protocol { get; set; }
    }
}