﻿using System.Linq;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Api.Abstractions.ResponseModels;
using Hjklvfr.Sueta.Grabber.Abstractions;
using Microsoft.AspNetCore.Mvc;

namespace Hjklvfr.Sueta.Grabber.Api.Controllers
{
    [ApiController]
    [Route("v1/grabber")]
    public class GrabberController : ControllerBase
    {
        private readonly IProxyGrabber _proxyGrabber;

        public GrabberController(
            IProxyGrabber proxyGrabber)
        {
            _proxyGrabber = proxyGrabber;
        }

        [HttpGet("find")]
        public async Task<ProxyListResponseModel> GetProxies()
        {
            var proxyResponseModels = (await _proxyGrabber.FindProxies())
                .Select(ProxyResponseModel.FromProxyModel).ToList();
            return new ProxyListResponseModel()
            {
                Length = proxyResponseModels.Count,
                ProxyResponseModels = proxyResponseModels
            };
        }
    }
}