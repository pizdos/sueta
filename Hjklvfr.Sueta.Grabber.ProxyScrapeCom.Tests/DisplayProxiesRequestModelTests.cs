﻿using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions.RequestModels;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Constants;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Extensions;
using Xunit;

namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Tests
{
    public class DisplayProxiesRequestModelTests
    {
        [Fact]
        public void ToQueryStringTest()
        {
            var query = new DisplayProxiesRequestModel()
            {
                ProxyType = ProxyTypeConstants.Http,
                Anonymity = AnonymityConstants.All,
                AverageTimeout = 100,
                Format = FormatConstants.Json,
                SerialKey = null,
                Ssl = SslConstants.All,
                Timeout = 10000
            }.ToQueryString();
            Assert.Equal("proxytype=http&timeout=10000&ssl=all&anonymity=all&country=all&limit=all&format=json&age=unlimited&status=alive&averagetimeout=100&port=all&request=displayproxies", query);
        }
    }
}