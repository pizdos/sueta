﻿using System.Threading.Tasks;
using Xunit;

namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Tests
{
    public class ProxyScrapeComGrabberTests : ProxyScrapeComClientTest
    {
        [Fact]
        public async Task FindProxiesTest()
        {
            var proxyScrapeComGrabber = new ProxyScrapeComGrabber(GetProxyScrapeComClient());
            Assert.NotEmpty(await proxyScrapeComGrabber.FindProxies());
        }
    }
}