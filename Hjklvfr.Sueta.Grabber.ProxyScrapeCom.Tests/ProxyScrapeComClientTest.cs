using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Abstractions.RequestModels;
using Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Constants;
using Xunit;

namespace Hjklvfr.Sueta.Grabber.ProxyScrapeCom.Tests
{
    public class ProxyScrapeComClientTest
    {
        public IProxyScrapeComClient GetProxyScrapeComClient()
        {
            var handler = new HttpClientHandler()
            {
                AutomaticDecompression = (DecompressionMethods.GZip | DecompressionMethods.Deflate)
            };
            var httpClient = new HttpClient(handler)
            {
                BaseAddress = ProxyScrapeComClient.BaseAddress
            };
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return new ProxyScrapeComClient(httpClient);
        }

        [Fact]
        public async Task GetProxiesTest()
        {
            var client = GetProxyScrapeComClient();
            var proxies = await client.GetProxies(new DisplayProxiesRequestModel()
            {
                ProxyType = ProxyTypeConstants.Http,
                Anonymity = AnonymityConstants.All,
                AverageTimeout = null,
                Format = FormatConstants.Normal,
                SerialKey = null,
                Ssl = SslConstants.No,
                Timeout = 10000
            });
            Assert.Empty(proxies);
        }
    }
}